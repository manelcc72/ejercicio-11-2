package es.manelcc.t11ej2;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements LocationListener {

    private EditText mEtLatitud, mEtLongitud, mEtPrecision;
    private TextView txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEtLatitud = (EditText) findViewById(R.id.latitud);
        mEtLongitud = (EditText) findViewById(R.id.longitud);
        mEtPrecision = (EditText) findViewById(R.id.precision);
        txt = (TextView) findViewById(R.id.direccion);

        LocationManager locationmanager;
        locationmanager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setSpeedRequired(false);
        criteria.setCostAllowed(true);

        String bestProvider = locationmanager.getBestProvider(criteria, true);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
        locationmanager.requestLocationUpdates(bestProvider, 1, 1, this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLocationChanged(Location location) {
        updateLocation(location);
        new GeocoderSearch().execute(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        updateLocation(null);
    }

    // Nuevo metodo para actualizar los datos de la posicion
    private void updateLocation(Location location) {
// Mostrar la hora para ver que se actualiza
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        String dt = dateFormat.format(cal.getTime());
        if (location != null) {
            mEtLatitud.setText(String.valueOf(location.getLatitude()));
            mEtLongitud.setText(String.valueOf(location.getLongitude()));
            mEtPrecision.setText(String.valueOf(location.getAccuracy()));
        } else {
            mEtPrecision.setText("No se ha podido establecer su posición a hora: " + dt);
        }
    }

    private class GeocoderSearch extends AsyncTask<Location, Void, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(Location... params) {
            Geocoder geo = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> destino = null;


            try {
                destino = geo.getFromLocation(params[0].getLatitude(), params[0].getLongitude(), 1);

            } catch (IOException e) {
                e.printStackTrace();
            }

            if (destino == null || destino.size() == 0) {
                return null;

            } else {

                StringBuilder lugarString = new StringBuilder();
                if (destino.get(0).getAddressLine(0) != null) {
                    lugarString.append(destino.get(0).getAddressLine(0));
                }

                if (destino.get(0).getAddressLine(1) != null) {
                    lugarString.append(";")
                            .append(destino.get(0).getAddressLine(1));
                }

                return lugarString.toString();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result != null) {


                txt.setText(result);


            }
        }
    }
}
